# 181-331_Kornienko_Ekaterina
# Описание

Экзамен 13.07.2020

Приложение "Интернет-магазин"

Передо мной стояла задача разработать мобильное приложение - клиент интернет-магазина такое , чтобы
 графический интерфейс содержал 2 страницы: “Каталог” и “Описание”, а также  боковую панель.

Данное приложение было написано 13.07.2020 года для сдачи экзамена в Московском политехническом университете по дисциплине "Разработка защищенных мобильных приложений".

Приложение "Интернет-магазин" предназначено для оформления заказов на мобильном устройстве.

Учащийся - Корниенко Екатерина Максимовна, учебная группа 181-331

kate_number_2000@mail.ru

mospolytech.ru

# Снимки
<img src="https://gitlab.com/sheep7182/181_331_kornienko/-/raw/master/img/Screenshot_4.png" width=300> 
<img src="https://gitlab.com/sheep7182/181_331_kornienko/-/raw/master/img/Screenshot_5.png" width=300>  
<img src="https://gitlab.com/sheep7182/181_331_kornienko/-/raw/master/img/Screenshot_6.png" width=300>

# О создании приложения
Приложение созданно в среде разработки QT, в качестве экзаменационной работы в формате WordSkils. 
Работа выполнялась 13.07.2020