import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtMultimedia 5.14
import QtQuick.Dialogs 1.3
import QtWebView 1.1
import QtWebSockets 1.5
import QtQuick.Window 2.2


ApplicationWindow {
    visible: true
    width: 700
    height: 600
    title: qsTr("Экзамен")

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex
        Drawer{
            id: draw
            width: parent.width * 0.5
            height: parent.height
            ColumnLayout{
                anchors.fill: parent
                Text{
                    id:magazin
                    text: "Интернет-магазин"
                    font.pixelSize: 5 * Screen.pixelDensity
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                Text{
                    id:student
                    text: "Корниенко Екатерина\nстудент 2 курса\nгруппы 181-331"
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 20
                }
                Image{
                    id:img
                    source: "qrc:/img/XDlKRshtcrs.jpg"
                    Layout.preferredWidth:  150
                    Layout.preferredHeight:  200
                    anchors.horizontalCenter: parent.horizontalCenter
                    fillMode: Image.PreserveAspectFit
                }
                Text{
                    id: mail
                    text: "Автор:\nkate_number_2000@mail.ru"
                    font.pixelSize: 20
                    anchors.horizontalCenter: parent.horizontalCenter

                }
                Text{
                    id: s_git
                    text: '<html><style type="text/css"></style><a href="https://gitlab.com/sheep7182/181_331_kornienko">https://gitlab.com/sheep7182/181_331_kornienko</a></html>'
                    anchors.horizontalCenter: parent.horizontalCenter
                }

            }
        }
        Page {
            id:katalog
            header:
                Item {
                height: 40

                Rectangle {
                    anchors.fill: parent
                    color:"#E6E6E6"
                }
                Button {
                    id:btn_menu
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    Rectangle {
                        anchors.fill: parent
                        color:"#E6E6E6"
                        Image {
                            height: 40
                            width: 70
                            source: "qrc:/img/unnamed.png"
                        }
                    }
                    onClicked: {
                        draw.open();
                    }
                }
                Label {
                    id:lab_katal
                    text: "Каталог"
                    font.family: "Arial"
                    font.pixelSize: 40
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.rightMargin:10
                }
                Button {
                    id:perecluch_plitka
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.margins: 5
                    anchors.right: perecluch_spisok.left
                    icon.color: "transparent"
                    icon.source: "qrc:/img/plitka.png"
                    onClicked: {
                        plitka.cellHeight = katalog.width/3.4
                        plitka.cellWidth = katalog.width/2.1
                    }
                    background: Rectangle {
                        color: "Silver"
                        border.width: 1
                        radius: 6
                    }
                }
                Button {
                    id:perecluch_spisok
                    anchors.top: parent.top
                    anchors.margins: 5
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    icon.color: "transparent"
                    icon.source: "qrc:/img/list.png"
                    onClicked: {
                        plitka.cellWidth = katalog.width
                        plitka.cellHeight = katalog.width/3
                    }
                    background: Rectangle {
                        color: "Silver"
                        border.width: 1
                        radius: 6
                    }
                }
            }

            Rectangle {
                anchors.margins: 5
                anchors.fill: parent
                color: "#e6e6e6"
                radius:10

                GridView {
                    id:plitka
                    visible: true
                    anchors.fill: parent
                    cellWidth : parent.width
                    cellHeight : katalog.width/3

                    delegate:
                        Rectangle {
                        color: "#57bfff"
                        border.color: "#2baeff"
                        RowLayout {
                            anchors.margins: 10
                            Rectangle {
                                id:kartinka
                                height: 150
                                width: 150
                                Image {
                                    id: image_1
                                    source: photo
                                    width: kartinka.width
                                    height: kartinka.width
                                }
                            }
                            ColumnLayout {
                                Label {
                                    id:tovar_1
                                    text: tovar
                                    font.pixelSize: 17
                                    font.bold: true
                                }
                                RowLayout{
                                    Label {
                                        id:price_1
                                        text: price
                                        anchors.right: parent.right
                                        font.pixelSize: 17
                                        font.bold: true
                                        color: "#696969"
                                    }

                                }
                                RowLayout{
                                    Button {
                                        id: but_1
                                        icon.color: "transparent"
                                        icon.source: "qrc:/img/263142.png"
                                        background: Rectangle {
                                            color: "#00000000"
                                        }
                                        onClicked: {
                                            swipeView.currentIndex = 1
                                            tovar_opis.text = tovar_1.text
                                            price_opis.text = price_1.text
                                            imq_opis.source = image_1.source
                                        }
                                    }
                                    Button {
                                        id: but_bolsh
                                        icon.color: "transparent"
                                        icon.source:"qrc:/img/inf.png"
                                        background: Rectangle {
                                            color: "#00000000"
                                        }
                                        onClicked: {
                                            swipeView.currentIndex = 1
                                            tovar_opis.text = tovar_1.text
                                            price_opis.text = price_1.text
                                            imq_opis.source = image_1.source
                                        }
                                    }
                                }
                            }
                        }
                    }

                    model: ListModel{
                        id: magaz_model
                        ListElement{
                            tovar: "Сумка чёрный лак"
                            price: "6 480р"
                            photo: "qrc:/img/1915.970.webp"
                        }
                        ListElement{
                            tovar: "Сумка серый металлик"
                            price: "4 730р"
                            photo:"qrc:/img/4950.970.webp"
                        }
                        ListElement{
                            tovar: "Сумка рептилия"
                            price: "6 180р"
                            photo:"qrc:/img/1898.970.webp"
                        }
                        ListElement{
                            tovar: "Сумка  бордо"
                            price: "3 380р"
                            photo:"qrc:/img/4159.970.webp"
                        }
                    }
                }
                Button{
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    anchors.margins: 5

                    background: Rectangle {
                        implicitWidth: 250
                        implicitHeight: 45
                        color: "Silver"
                        radius: 15
                    }
                    Text{
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: img.left
                        text: qsTr("Перейти в корзину")
                        font{
                            family: "Tahoma"
                            pixelSize: 20
                        }
                    }

                    onClicked: {
                        swipeView.currentIndex = 2
                    }

                }
            }
            /*ColumnLayout{
                anchors.fill: parent
                GridView {
                    id:spisok
                    visible: true
                    anchors.fill: parent
                    cellWidth: katalog.width
                    cellHeight: katalog.width*0.5

                    delegate: Rectangle {
                        ColumnLayout {
                            RowLayout {
                                Rectangle {
                                    id:kartinka_1
                                    height: 150;
                                    width: 150;
                                    Image {
                                        id: image_1
                                        source: photo
                                        width: kartinka_1.width
                                        height: kartinka_1.width

                                    }
                                }
                                ColumnLayout {
                                    Label {
                                        text: tovar
                                        font.pixelSize: 11
                                        font.bold: true
                                    }
                                    RowLayout{
                                        anchors.right: parent.right
                                        Label {
                                            text: price
                                            font.pixelSize: 11
                                            font.bold: true
                                            color: "#9f9f9f"
                                        }
                                        Button {
                                            id: but_2
                                            icon.color: "transparent"
                                            icon.source: "qrc:/img/263142.png"
                                            background: Rectangle {
                                                color: "#00000000"
                                            }
                                        }
                                    }
                                }
                            }

                        }

                    }
                    model: ListModel{
                        id: magaz_model2
                        ListElement{
                            tovar: "Сумка женская из натуральной кожи A.Valentino - 428 чёрный лак"
                            price: "6 480р"
                            photo: "qrc:/img/1915.970.webp"
                        }
                        ListElement{
                            tovar: "Женская сумка из натуральной кожи Protege Ц-329 серый металлик"
                            price: "4 730р"
                            photo:"qrc:/img/4950.970.webp"
                        }
                        ListElement{
                            tovar: "Сумка женская из натуральной кожи Protege Ц-110 бордовая рептилия"
                            price: "6 180р"
                            photo:"qrc:/img/1898.970.webp"
                        }
                        ListElement{
                            tovar: "Сумка из натуральной кожи A.Valentino - 368 бордо"
                            price: "3 380р"
                            photo:"qrc:/img/4159.970.webp"
                        }
                    }
                }
            }*/
        }

        Page {
            header:
                ToolBar{
                anchors.top: parent.top

                Rectangle{
                    implicitHeight: 50
                    width: parent.width
                    color:"#E6E6E6"

                    Label {
                        text: "Описание товара"
                        font.family: "Arial"
                        font.pixelSize: 40
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.rightMargin:10
                    }
                    Button{
                        id:back
                        icon.color:"transparent"
                        icon.source: "qrc:/img/16116.png"
                        background: Rectangle {
                            color: "#00000000"
                        }
                        onClicked:{
                            swipeView.currentIndex = 0
                        }
                    }
                }
            }
            ColumnLayout{
                anchors.fill: parent
                anchors.horizontalCenter: parent.horizontalCenter
                Button{
                    id: control
                    anchors.margins: 5
                    anchors.right: parent.right

                    background: Rectangle {
                        implicitWidth: 250
                        implicitHeight: 45
                        color: "Silver"
                        border.width: 1
                        radius: 6
                    }
                    Text{
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: qsTr("Перейти в корзину")
                        font{
                            family: "Tahoma"
                            pixelSize: 20
                        }
                    }

                    onClicked: {
                        swipeView.currentIndex = 2
                    }
                }
                Rectangle {
                    id:kartink
                    Layout.fillWidth:true
                    Layout.preferredHeight: 400

                    Image{
                        id:imq_opis
                        fillMode: Image.PreserveAspectFit
                        anchors.fill:parent
                    }
                }

                Label{
                    id: tovar_opis
                    font.pixelSize: 40
                    font.bold: true
                }

                Label{
                    id: price_opis
                    font.pixelSize: 40
                    anchors.right: parent.right
                }


            }

        }
        Page {
            header:
                Item {
                height: 40

                Rectangle {
                    anchors.fill: parent
                    color:"#E6E6E6"
                }
                Label {
                    text: "Корзина"
                    font.family: "Arial"
                    font.pixelSize: 40
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.rightMargin:10
                }
                Button {
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: lab_katal.right
                    anchors.right: perecluch_spisok.left
                    icon.source: "qrc:/img/16116.png"
                    background: Rectangle {
                        color: "#00000000"
                    }
                    onClicked:{
                        swipeView.currentIndex = 0
                    }
                }
            }


        }


    }
    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Каталог")
        }
        TabButton {
            text: qsTr("Описание")
        }
        TabButton {
            text: qsTr("Корзина")
        }
    }
}
